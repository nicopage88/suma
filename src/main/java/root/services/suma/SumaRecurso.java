package root.services.suma;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/")
public class SumaRecurso {

    @GET
    @Path("/suma")
    public String getSumaQuery(@QueryParam("numeros") String numeros) {
        String listaNumeros[] = numeros.split(",");
        Integer suma = 0;
        for (int i = 0; i < listaNumeros.length; i++) {
            //String ListaNumero = ListaNumeros[i];
            suma = suma + Integer.parseInt(listaNumeros[i]);

        }
        return "El resultado de lasuma es" + suma.toString();
    }
}
